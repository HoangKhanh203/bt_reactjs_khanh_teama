
import React, { Component } from 'react';
import './App.css';
import User from './components/User';
import Picture from './components/Picture';

class App extends Component 
{
  constructor(props) 
  {
    super(props);
    this.state = {
      showInput: false,
      NewInfor:
      {
        name: '',
        age: '',
        job: '',
      },
      newName: '',

      userInfor: {
        name: 'Quynh Nguyen 1',
        age: 28,
        job: 'Front-end Dev'
      }
    }
  }



  handleShowInput = () => {
    this.setState({
      showInput: !this.state.showInput
    })
  }
 
  /*------------New------------*/ 
  handleChangeName = (New) => {
    const value = New.target.value;
    this.setState ({
      NewInfor :{
      ...this.state.NewInfor,
        name : value,
      }
    });
  }
  handleChangeAge = (New) => {
    const value = New.target.value;
    this.setState ({
      NewInfor :{
      ...this.state.NewInfor,
        age : value,
      }
    });
  }
  handleChangeJob = (New) => {
    const value = New.target.value;
    this.setState ({
      NewInfor :{
      ...this.state.NewInfor,
        job : value,
      }
    });
  }
  onChangeValue = () =>{
    this.setState ({
      userInfor: {
          name : this.state.NewInfor.name,
          age : this.state.NewInfor.age,
          job : this.state.NewInfor.job,
      }
    });
  }
 

  
    /*------------New------------*/ 
  render() {
    const { name, age, job } = this.state.userInfor;
    
    return (
      <div className="App">

        <i className="fa fa-heart" onClick={this.handleShowInput} />
        {
          this.state.showInput && (
            <div style={{
              padding: '5px'
            }}>

              <input  placeholder="Input your name" onChange= { this.handleChangeName} /> 
              <input  placeholder="Input your name" onChange= { this.handleChangeAge} /> 
              <input  placeholder="Input your name" onChange= { this.handleChangeJob} /> 

 
              <button onClick={this.onChangeValue} >Change Name</button>
            </div>
          )
        }
        <User name={name} age={age} job={job} />  
        <Picture imgSrc={'http://dep.com.vn/wp-content/uploads/avengers-va-avatar-4.jpg'} />
      </div>
    )
  }
}

export default App;