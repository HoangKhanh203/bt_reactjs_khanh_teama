import React, { Component } from 'react';

class Picture extends Component {
  render() {
    const { imgSrc } = this.props; 
    const styleObject = {
      textAlign: 'left',
      paddingLeft: 500,
    }
    return (
      <div style={styleObject}>
        <img alt="hinh" style={{
          width: "30%",
          height: "30%",
        }} src={imgSrc} />
      </div>
    )
  }
}

export default Picture;
