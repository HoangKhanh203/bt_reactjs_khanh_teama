import React, { Component } from 'react';

class User extends Component {
  render() {
    const { name, age, job } = this.props;
    const styleObject = {
      textAlign: 'left',
      paddingLeft: 500,
    }
    return (
      <div style={styleObject}>
        <div>
          <span>Name: {name}</span>
        </div>
        <p>Age: {age}</p>
        <p>Job: {job}</p>
      </div>
    )
  }
}

export default User;